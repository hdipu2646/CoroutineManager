[v2.0.0](#v2.0.0)
### Added
- Addded a onCoroutineQueueIsFree Event calling Dynamically after <param name="coroutines"></param> queue is Free after Execute.
- Create Clear all pending task on onCoroutineQueueIsFree Events.
- Don't forget to RemoveAllListerner After Perfom a Action.

### Changes
- Commenting for User Guides.

[v1.0.1](#v1.0.1)
### Added
- Add Reset Coroutine Manager Function

### Changes
- StartCoroutineSequence Function changes to private
- StartCoroutineSequence will be starting from Reset Function

[v1.0.0](#v1.0.0)