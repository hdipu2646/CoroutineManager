using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Walid.System.Generic
{
    /*// Scheduler
    class Sched
    {
        // simple singleton
        private static Sched instance = new Sched();
        public static Sched Instance
        {
            get { return instance; }
        }

        public class Coroutine
        {
            public IEnumerator routine;
            public Coroutine waitForCoroutine;
            public bool finished = false;

            public Coroutine(IEnumerator routine) { this.routine = routine; }
        }

        List<Coroutine> coroutines = new List<Coroutine>();

        private Sched() { }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            Coroutine coroutine = new Coroutine(routine);
            coroutines.Add(coroutine);

            return coroutine;
        }

        public void Update()
        {
            foreach (Coroutine coroutine in coroutines.Reverse<Coroutine>())
            {
                if (coroutine.routine.Current is Coroutine)
                    coroutine.waitForCoroutine = coroutine.routine.Current as Coroutine;

                if (coroutine.waitForCoroutine != null && coroutine.waitForCoroutine.finished)
                    coroutine.waitForCoroutine = null;

                if (coroutine.waitForCoroutine != null)
                    continue;

                // update coroutine

                if (coroutine.routine.MoveNext())
                {
                    coroutine.finished = false;
                }
                else
                {
                    coroutines.Remove(coroutine);
                    coroutine.finished = true;
                }
            }
        }
    }*/

    public class CoroutineManager : MonoBehaviour
    {
        public Queue<IEnumerator> coroutines;
        private int runnedCoroutines = 0;

        [Serializable]
        /// <summary>
        /// Function definition for when Coroutine Manager's Queue is Free Event.
        /// </summary>
        public class CoroutineManagerQueueFreeEvent : UnityEvent { }
        // Event delegates triggered on Coroutine Manager's Queue is Free.
        [FormerlySerializedAs("onCoroutineQueueIsFree")]
        [SerializeField]
        private CoroutineManagerQueueFreeEvent m_OnCoroutineIsFree = new CoroutineManagerQueueFreeEvent();

        protected CoroutineManager()
        {}


        /// <summary>
        /// UnityEvent that is triggered when the Coroutine Queue is free.
        /// Note: Triggered on Dynamically after <param name="coroutines"></param> queue is Free queue is Free after Execute. And Also Don't forget to RemoveAllListerner After Perfom a Action.
        /// </summary>
        ///<example>
        ///<code>
        /// <![CDATA[
        /// using UnityEngine;
        /// using UnityEngine.UI;
        /// using System.Collections;
        ///
        /// public class QueueFreeExample : MonoBehaviour
        /// {
        ///     public Button yourButton;
        ///
        ///     void Start()
        ///     {
        ///         CoroutineManager.Singleton.onCoroutineQueueIsFree.AddListener(TaskAfterCoroutineQueueIsFree);
        ///     }
        ///
        ///     void TaskAfterCoroutineQueueIsFree()
        ///     {
        ///         //Task
        ///         Debug.Log("You have clicked the button!");
        ///         //Clear all pending task on onCoroutineQueueIsFree Events.
        ///         ClearOnCoroutineQueueIsFreeEvent();
        ///     }
        /// }
        /// ]]>
        ///</code>
        ///</example>
        public CoroutineManagerQueueFreeEvent onCoroutineQueueIsFree
        {
            get
            {
                return m_OnCoroutineIsFree;
            }
            set {
                m_OnCoroutineIsFree = value;
            }
        }

        #region Singleton
        private static CoroutineManager _instance;
        public static CoroutineManager Singleton
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<CoroutineManager>();
                    if (_instance == null)
                    {
                        GameObject coroutineManager = new GameObject();
                        coroutineManager.name = typeof(CoroutineManager).Name;
                        _instance = coroutineManager.AddComponent<CoroutineManager>();
                        DontDestroyOnLoad(coroutineManager);
                    }
                }
                return _instance;
            }
        }

        public static void SetNewSingleTon(CoroutineManager coroutineManager)
        {
            if (_instance != null)
            {
                return;
            }
            _instance = coroutineManager;
        }
        #endregion Singleton

        private IEnumerator StartCoroutineSequence(bool showDebugMessage)
        {
            while (true)
            {
                while (coroutines.Count > 0)
                {
                    IEnumerator runningCoroutine;
                    coroutines.TryDequeue(out runningCoroutine);
                    yield return StartCoroutine(runningCoroutine);
                    if (showDebugMessage)
                        Debug.Log(string.Format("Total Coroutine runs: {0}", runnedCoroutines++));
                }
                m_OnCoroutineIsFree?.Invoke();
                yield return new WaitForEndOfFrame();
            }
        }

        /// <summary>
        /// To reset queue and restart the Coroutine Manager, Also Clear all pending task on onCoroutineQueueIsFree Events.
        /// </summary>
        /// <param name="showDebugMessage">If true it will show how many coroutine runs after calling this Reset function</param>
        public void Reset(bool showDebugMessage)
        {
            StopAllCoroutines();
            ClearOnCoroutineQueueIsFreeEvent();
            runnedCoroutines = 0;
            coroutines = new Queue<IEnumerator>();
            StartCoroutine(StartCoroutineSequence(showDebugMessage));
        }

        /// <summary>
        /// To Clear all pending task on onCoroutineQueueIsFree Events.
        /// </summary>
        public void ClearOnCoroutineQueueIsFreeEvent()
        {
            m_OnCoroutineIsFree = new CoroutineManagerQueueFreeEvent();
        }
    }
}